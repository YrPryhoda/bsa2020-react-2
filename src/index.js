import React from "react";
import ReactDOM from "react-dom";
import Chat from "./components/Chat";
import {Provider} from 'react-redux';
import store from './redux/store';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Chat />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
