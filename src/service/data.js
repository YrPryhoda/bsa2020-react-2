import {
  API_URL
} from './constants';

export const getApiData = () => {
  return fetch(API_URL)
    .then(data => data.json())
}