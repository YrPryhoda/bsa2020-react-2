import {v4} from 'uuid';

export const userModel = () => ({
  id: v4(),
  text: '',
  user: 'Admin',
  avatar: '',
  userId: v4(),
  editedAt: null,
  createdAt: (new Date()).toJSON()
})