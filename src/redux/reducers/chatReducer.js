import {
  UPDATE_DATA,
  SEND_MESSAGE,
  SHOW_MODAL,
  CLOSE_MODAL,
  EDITE_MESSAGE,
  DELETE_MESSAGE,
  REACT_MESSAGE
} from '../actionTypes';

export default (state = {}, action) => {
  const {
    type,
    payload
  } = action;
  switch (type) {
    case UPDATE_DATA:
      return {
        messageData: payload
      }
    case SEND_MESSAGE:
      const newData = [
        ...state.messageData,
        payload
      ]
      return {
        messageData: newData
      }
    case SHOW_MODAL:
      const message = state.messageData.find(el => el.id === payload);
      return {
        messageData: [...state.messageData],
          expandedMessage: message
      }
    case CLOSE_MODAL:
      return {
        messageData: [...state.messageData]
      }
    case EDITE_MESSAGE:
      const index = state.messageData.findIndex(el => el.id === payload.id);
      const newArray = [
        ...state.messageData.slice(0, index),
        payload,
        ...state.messageData.slice(index+1)
      ]
      return {
        messageData: [...newArray]
      }
    case DELETE_MESSAGE:
      const newArr = state.messageData.filter(el => el.id !== payload);
      return {
        messageData: [...newArr]
      }
    case REACT_MESSAGE:
      const idx = state.messageData.findIndex(el => el.user === payload.user && el.text === payload.text);
      const post = {...state.messageData[idx]};
      post.likes = 1;
      const newAr = [
        ...state.messageData.slice(0, idx),
        post,
        ...state.messageData.slice(idx+1)
      ]
      return {
        messageData: [...newAr]
      }
      default:
        return state;
  }
};