import {
  userModel
} from '../service/helper/userTemplate';

import {
  UPDATE_DATA,
  SEND_MESSAGE,
  SHOW_MODAL,
  CLOSE_MODAL,
  EDITE_MESSAGE,
  DELETE_MESSAGE,
  REACT_MESSAGE
} from './actionTypes';

export const fetchData = data => {
  return {
    type: UPDATE_DATA,
    payload: data
  }
};

export const sendMessage = (text) => {
  const payload = {
    ...userModel(),
    text
  }
  return {
    type: SEND_MESSAGE,
    payload
  }
};

export const editOwnPost = (id) => ({
  type: SHOW_MODAL,
  payload: id
})

export const onModalClose = () => ({
  type: CLOSE_MODAL
})

export const sendEditedMsg = (data) => ({
  type: EDITE_MESSAGE,
  payload: data
})

export const deleteOwnMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: id
})

export const setLike = (user, text) => ({
  type: REACT_MESSAGE,
  payload: {user, text}
})