import {
  combineReducers,
  createStore
} from 'redux';
import {
  composeWithDevTools
} from 'redux-devtools-extension';
import chatReducer from './reducers/chatReducer';

const initialState = {}
const rootReducer = combineReducers({
  chatReducer
})
const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools()
);

export default store;