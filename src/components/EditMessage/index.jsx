import React, { useState } from 'react'
import './editMessage.css';

const EditMessage = ({ expandedMessage, sendEditedMsg, onModalClose }) => {
  const confirmEdit = (text, e) => {
    e.preventDefault();
    const newBody = {
      ...expandedMessage,
      text
    }
    sendEditedMsg(newBody);
    onModalClose();
  }
  const [input, setInput] = useState(expandedMessage.text);
  const onChange = e => setInput(e.target.value);
  return (
    <div className="abs-wrapper">
      <div className="modal">
        <h2>{expandedMessage.user}</h2>
        <form onSubmit={e => confirmEdit(input, e)} className='edit-form'>
          <textarea cols="30" rows="4" value={input} onChange={e => onChange(e)}>
          </textarea>
          <input type="submit" className="pointer" value="Edit" />
          <i className="abs-close fas fa-times" onClick={onModalClose}></i>
        </form>
      </div>
    </div>
  )
}

export default EditMessage;
