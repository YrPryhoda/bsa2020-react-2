import React, { useEffect } from "react";
import MessageList from "../MessageList";
import MessageInput from "../MessageInput";
import Header from "../Header";
import Spinner from '../Spinner';
import Footer from '../Footer';
import EditMessage from '../EditMessage';
import UsersList from "../UsersList";
import {
  fetchData,
  sendMessage,
  sendEditedMsg,
  onModalClose
} from "../../redux/actions";
import { getApiData } from '../../service/data';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import moment from 'moment';
import "./chat.css";

function App({
  fetchData,
  chatData,
  sendMessage,
  expandedMessage,
  sendEditedMsg,
  onModalClose
}) {
  useEffect(() => {
    getApiData().then(data => {
      const newArr = data.sort((a,b) => moment(a.createdAt, "hh:mm:ss") - moment(b.createdAt, "hh:mm:ss"));
      console.log(newArr);
      fetchData(newArr)
    })
  }, []);


  return !chatData ? (
    <Spinner />
  ) : (
      <div className="App">
        <Header state={chatData} />
        <div className="main">
          <div className="left">
            <MessageList />
            <MessageInput sendMessage={sendMessage} />
          </div>
          <UsersList state={chatData} />
        </div>
        {
          expandedMessage && <EditMessage
            expandedMessage={expandedMessage}
            sendEditedMsg={sendEditedMsg}
            onModalClose={onModalClose} />
        }
        <Footer />
      </div>
    );
}
const mapStateToProps = state => ({
  user: state.userReducer,
  chatData: state.chatReducer.messageData,
  expandedMessage: state.chatReducer.expandedMessage
});
const mapDispatchToProps = dispatch => ({
  fetchData: bindActionCreators(fetchData, dispatch),
  sendMessage: bindActionCreators(sendMessage, dispatch),
  sendEditedMsg: bindActionCreators(sendEditedMsg, dispatch),
  onModalClose: bindActionCreators(onModalClose, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(App);
