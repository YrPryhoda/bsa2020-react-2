import React from 'react'
import './usersList.css';

const UsersList = ({ state }) => {
  const getUsersList = (data) => {
    let uniqueUsers = [];
    for (let item of data) {
      if (!uniqueUsers.includes(item.user)) {
        uniqueUsers.push(item.user)
      }
    }
    return uniqueUsers;
  }
  return (
    <div className="users-list-block">
      <h4>Registered users:</h4>
      <ul>
        {
          getUsersList(state).map(el => <li key={el}> {el} </li>)
        }
      </ul>
    </div>
  )
}

export default UsersList;
