import React, { useRef, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment';
import {
  editOwnPost,
  deleteOwnMessage,
  setLike
} from "../../redux/actions";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import './messageList.css';

function MessageList({ messages, editOwnPost, deleteOwnMessage, setLike }) {
  const messanger = useRef(null);
  const [showEditBtn, setShowEditBtn] = useState(false);
  useEffect(() => {
    messanger.current.scrollTo(0, messanger.current.scrollHeight);
  }, [messanger.current && messanger.current.scrollHeight]);
  const getMessageList = data => {
    return data.map(el => {
      return (
        <div
          onMouseEnter={() => setShowEditBtn(true)}
          onMouseLeave={() => setShowEditBtn(false)}
          className={`${el.user === 'Admin' ? "message-block right" : "message-block"}`}
          key={el.createdAt}
          id={el.createdAt}
        >

          {el.user !== 'Admin' && (<img
            src={el.avatar}
            className="message-avatar"
            alt='users avatar' />)}

          <div className="message-body">
            <h3 className="user-name">
              {el.user}
            </h3>
            <div className="message-text">
              {el.text}
            </div>
            <div className="message-meta">
              <div className="message-react">
                {
                  el.user !== 'Admin' && (
                    <>
                      <span>{el.likes}</span>
                      <span
                        className="pointer fa fa-thumbs-o-up"
                        aria-hidden="true"
                        onClick={() => { setLike(el.user, el.text) }}
                      >
                      </span>
                    </>
                  )
                }
                {
                  el.user === 'Admin' && showEditBtn && (
                    <span
                      className="pointer fas fa-edit my-edit-btn"
                      onClick={() => { editOwnPost(el.id) }}
                    >
                    </span>
                  )}
                {
                  el.user === 'Admin' && <span
                    className="pointer far fa-trash-alt"
                    onClick={() => deleteOwnMessage(el.id)}
                  >
                  </span>
                }
              </div>
              <div className="message-date">
                {moment(el.createdAt).format("DD MM YYYY hh:mm:ss")}
              </div>
            </div>
          </div>
        </div>
      )
    })
  }
  return (
    <div className='messages' ref={messanger}>
      {getMessageList(messages)}
    </div>
  )
}

MessageList.propTypes = {
  messages: PropTypes.array.isRequired
}
const mapStateToProps = (state) => ({
  messages: state.chatReducer.messageData
})

const mapDispatchToProps = dispatch => ({
  editOwnPost: bindActionCreators(editOwnPost, dispatch),
  deleteOwnMessage: bindActionCreators(deleteOwnMessage, dispatch),
  setLike: bindActionCreators(setLike, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
